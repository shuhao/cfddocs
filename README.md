CFD Documentations
==================

Here lies all documentation for running and managing CFD simulations. Right now
only CFX simulations are to be considered.

To write these documentations, consult [Sphinx][sphinx], as that is the 
generation mechanism.

[sphinx]: http://www.sphinx-doc.org

