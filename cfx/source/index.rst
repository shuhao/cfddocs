========================
Getting Started with CFX
========================

Before reading this documentation, you need to complete some of the tutorials
from CFX's help menu until you feel relatively comfortable with the software.
At this point, you might feel that running a CFD simulation is quite an easy
task. To some extent, you are correct. CFX makes it extremely easy to get started
with simple simulations. However, running simulations on a cluster has its own
challenges.

This documentation aims to get you up to speed to how to setup a simple mesh,
setup the server, and run the simulation on the server. The documentation is
split into 3 different parts: Part I will describe how a simulation can be
setup, Part II will describe how to setup and run the simulation on the server,
Part III will describe how to get data and post process them.

In the future, this guide will update with a part 4, where you run simulations
with more automation so you can use focus your time on simulation better. This
requires a more advanced understanding of computers, however.

This documentation may be out of date. The latest compiled documentation
can be found at https://shuhao.gitlab.io/cfddocs/. The source code for this
can be found at https://gitlab.com/shuhao/cfddocs.

.. toctree::
   :caption: Table of Contents
   :maxdepth: 3

   partI
   partII
   partIII
