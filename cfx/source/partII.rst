.. _partII:

===============================================
Part II: Running CFX Simulation on Linux Server
===============================================

At this point it is assumed you know how to run a simulation locally on a
Windows machine. If not, go back to the CFX tutorial as it tells you how to do
this.

It is highly recommended that you thoroughly read through the General Workflow
section of this chapter before attempting to start a simulation.

.. include:: /partII/01-general-workflow.rst
.. include:: /partII/02-setting-up-server.rst
.. include:: /partII/03-running-simulation.rst
.. include:: /partII/04-monitoring-simulation.rst
.. include:: /partII/05-disaster-recovery.rst

