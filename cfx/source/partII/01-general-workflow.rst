General workflow
----------------

Unlike a simulation on your local machine, simulation on Linux servers/clusters
can take many weeks/months. No window needs to be kept open for the simulation
to run and these servers will be on 24/7 while the simulation is running.

Since Linux servers do not provide a graphical user interface (GUI) by default,
users should be somewhat familiar with commandline utilities or the standard
utilities provided by the Linux distribution. The end of this chapter contains
more information on basic Linux commands you should know.

At this point in time, you should have at least simulation defined in CFX Pre.
This can be done simply on a Windows computer with CFX installed. Once this
is defined, you must export a run definition file. To do this, you can click
the ``Tools`` menu and select ``Write Solver Input File``. This will save a .def
file.

This def file, along with boundary condition input files (known as User profiles),
are all you need to start a simulation. The CFX pre file (.cfx) is not required.

Simulation Workflow
+++++++++++++++++++

From this point forward, the general steps of running a simulation is the
following:

1. Copying the definition file and user profile files to the simulation server.
2. Connect to the server and get a shell session.
3. If you used custom profiles for boundary conditions with CSV files, the reference to these files are stored as an absolute path in the ``.def`` file. If you setup the simulation on Windows, the path saved would be something like ``C:\Users\JohnSmith\Documents\inlet-profile.csv``. This path does not exist on the simulation server and the ``.def`` file must be modified to match the path on the simulation server. This can be done using the text editor and ``cfx5cmds``. This step is only required if you used a custom profile. Otherwise, it is safe to skip this step.
4. Write (modify an existing) run script that helps with running the simulation.
5. Run the simulation in the **background** with ``tmux``. This is required as any program you start normally will be terminated the moment you disconnect from your remote login session. To get around this, tmux allows you to start a program and **detach** from it without terminating it when you log out.
6. (OPTIONAL) Start the automatic transient file mover. This step is only required if your simulation is very long and will contain more than 100GB of storage. This program is something I wrote that automatically moves your transient timesteps to an external NAS (network attached storage) where we have more storage capability.
7. Perform monitoring steps, as outlined below in the Monitoring Workflow.
8. Finish the simulation, move all the files to NAS and delete them on the simulation cluster so other students have space to complete their simulation.

Monitoring Workflow
+++++++++++++++++++

After you start the simulation, you need monitor it to ensure that it runs smoothly,
without errors and crashes. To do this, you need to do the following:

* Follow the simulation log file just like the CFX Solver Manager and ensure it
  does not have any bad warnings or errors.
* Ensure the CFX process is running on the server and did not die without your
  knowledge.
* (OPTIONAL) Ensure the automatic transient file mover is running and did not die without
  your knowledge.
* Ensure there are enough disk space on disk, in case the simulation are going
  faster and the transient file mover is too slow.
* Ensure the NAS have enough space.

The rest of this part will teach you how to perform all of these tasks
with some software I wrote that helps you along called cfdbox.
