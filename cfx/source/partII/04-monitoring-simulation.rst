Monitoring the Simulation
-------------------------

Now your CFX simulation should be under way. You're not done. Next we will setup
automatic transfer of transient files to the NAS and learn how to effectively
monitor the simulation for anomalies and crashes.

This again is a very basic tutorial. Part IV includes a way of automatic
monitoring where a system will automatic move files, watch disk space usage,
watch to see if the CFX process crashed, as well as send a daily progress
update about the speed of the simulation and the ETA. This requires some in
depth understanding of the system, however.

Generic Monitoring Tricks
+++++++++++++++++++++++++

In CFX Solver Manager there's an output of the logs telling you about which
iteration it is on and what the residuals are. This can be viewed by reading the
log file which would be named something like ``your_simulation_001.log``. If
you ran the simulation multiple times, the number ``001`` may increase. This file
should contain errors and should stop updating if the simulation stopped.

To view this file on the server as it updates in real time, in the simulation
directory, type (if your definition file is called ``your_simulation.def``):

.. code-block:: console

    $ tail -f your_simulation_001.log

You should see the output stream to your terminal. To see the entire file in
its entirity, use:

.. code-block:: console

    $ less your_simulation_001.log

You can use arrow keys, page up, page down, home, and end to scroll. Press ``q``
to quit.

If you want to abort a run, do not kill the process. Instead, use the following
command:

.. code-block:: console

    $ cfx5stop -directory your_simulation_001.dir

Again, change the number and the name accordingly. The name passed after the
``-directory`` flag needs to be a valid directory and it should be for the
current run.

To create a backup file:

.. code-block:: console

    $ cfx5control your_simulation_001.dir -backup

To check if CFX is still running:

.. code-block:: console

    $ ps aux | grep cfx

If you see a lot of output, then CFX is probably running. If you see
something like the following, CFX likely has stopped:

.. code-block:: console

    $ ps aux | grep cfx
    shuhaowu 63932  0.0  0.0 103252   856 pts/2    S+   16:04   0:00 grep cfx

To check for the amount of disk space remaining, from the simulation directory:

.. code-block:: console

    $ df -h .
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/md1        917G  828G   43G  96% /files1

As you can see from above, the message is fairly intuitive.

To monitor effectively with only the basic set of tools, I recommend you to log
on to the server **at least every other day** and perform the list of monitoring
steps as outlined in `Monitoring Workflow`_.


Setup Automatic Transient File Transfer
+++++++++++++++++++++++++++++++++++++++

The information below is out of date. I will be working on this later.

If your simulation writes transient files, you should periodically move it off
the cluster and put it into NAS. If the cluster runs out of disk space, CFX
**will crash** and it will **corrupt certain data**. Worst case, it may make
the simulation **unrecoverable**.

You can move the files manually using ``sftp``. However, this may be very
undesirable as you may have to do this once an hour, even during the middle
of the night, depending on the size of the simulation. This is due to the size
limitations of the cluster. That said, even if we have more space, this will
be an on going issue and you must make sure to move before it runs out of space,
which may be tricky to time.

To avoid this situation, I developed an automatic transient file mover to
automatically move the files to NAS and verify it at some given time interval
(default to 1 hour).

This script is a part of my advanced ``cfx/runsupport`` package which is not
yet ready for distribution at this time. I have, however, extracted the particular
script so it can be used.

This software comes in a package to install it, download it, upload it to the
server's home directory and run the following commands:

.. code-block:: console

    $ cd ~
    $ tar xvzf trn-file-mover.tar.gz
    $ cd trn-file-mover
    $ ./setup.sh

This will install the software. To use it, copy the file ``start_trn_move.sh``
from the ``trn-file-mover`` directory to your simulation directory. Run it with:

.. code-block:: console

    $ ./start_trn_move.sh your_simulation_001.dir >mover.stdout 2>mover.stderr &
    $ disown

Again, this must be in the background.

To ensure this thing is running correctly as it may crash every once in a while,
do:

.. code-block:: console

    $ ps aux | grep move_trn_via_sftp

You must see at least two entries here. If you only see the grep move_trn_via_sftp
entry, the thing is not running and you should restart it.
