Running the Simulation
----------------------

At this point, you should have your simulation files on the server, in a
directory of your choosing.

Running the Simulation in the Background with tmux
++++++++++++++++++++++++++++++++++++++++++++++++++

Before we get into actually starting the simulation, we must clarify a
**very important** aspect of running simulation: if you start a simulation
directly from a SSH session and if you disconnect (by closing the window, or
losing internet connection), the simulation will be terminated. This is a
feature as your simulation is running in the foreground and all foreground
programs will be terminated when you disconnect (a.k.a. logout).

This can be easily avoided. When you installed cfdbox, a program named tmux_.
Tmux is a program that can be put into background and not be terminated, as it
is now a background application.

.. _tmux: https://tmux.github.io/

This doc will provide a simple tutorial of it. You can learn more about the
program on the internet as it is very powerful. My personal simulation
interface is done via tmux and it looks like the following (this is not
something we will learn to do here as it is much more complicated):

.. image:: _static/tmux-demo.png

With tmux, you can start a tmux session with your SSH session. This session
is automatically started in the background and tmux automatically _attaches_ to
it. You can check if you have any session via ``tmux ls``:

.. code-block:: console

    $ tmux ls

If you don't have any sessions, you should see ``failed to connect to server: Connection refused``.
If you have sessions, you might see something like 0: 1 windows (created Sun Jun 26 15:07:55 2016) [173x45] (attached)``.

If this is your first time using tmux, or if you have no sessions already
running, you can start a new session by calling:

.. code-block:: console

    $ tmux

After, you should see a bar show up on the bottom of your screen like the following:

.. image:: _static/tmux-start.png

Note the ``[0]`` at the bottom left corner of the screen, it indicates that this
is the only tmux session in play (this is the 0th session). Tmux supports
multiple session however we should only use one. If you see ``[1]``. Execute ``exit``
to quit the session.

To prove that programs are kept if we logout, we can test it by just listing
the current directory, detach from the session, logout, log back in, and
reattaching to the session.

#. Execute ``ls`` within tmux and press enter to get an output.
#. Press ``CTRL+b``. Lift your fingers away from the keyboard. Now press only ``d``. This detaches you from the current tmux session. You should see the bar at the bottom of your terminal disappear, along with all the output of the ``ls`` we ran in step 1.
#. Logout by typing and executing ``exit``.
#. Log back into the same server.
#. Execute ``tmux attach`` again. You should once again see the bar at the bottom as well as the output of the ``ls`` we executed in step 1.

The important 3 steps here to remember is:

* ``tmux``: **spawns** a new tmux session.
* Pressing ``CTRL+b``, and then press ``d``, this will be refer this as ``CTRL+b d`` (note the space between): this **detaches** you from the tmux session.
* ``tmux attach``: which **attaches** you to an already existing tmux session.

When you run your simulation, it is important to you **always** run it within
tmux. You can check this by seeing if there is a bar at the bottom of your session
with a string of ``[0]`` at the bottom left corner. If you see ``[1]``, you should
terminate the tmux session with ``exit`` to avoid confusions.


Starting the Simulation
+++++++++++++++++++++++

You should put all of your simulations files under one folder on the the cluster.
I recommend you first create a folder called ``simulations`` inside your home
directory:

.. code-block:: console

    $ cd $HOME
    $ mkdir simulations

You can then put individual simulations inside the ``simulations`` folder.
As an example, we will create a folder called ``my-sim1``:

.. code-block:: console

    $ cd simulations
    $ mkdir my-sim1

One you do this, you can transfer your simulation definition file, boundary
profile files, and any initial condition files into ``$HOME/simulations/my-sim1``.

You then should copy the run script from cfdbox:

.. code-block:: console

    $ cd $HOME/simulations/my-sim1
    $ cp $HOME/cfdbox/scripts/start-simulation.sh .

You should modify this file with something known as _nano:

.. _nano: https://www.nano-editor.org/docs.php

.. code-block:: console

    $ nano start-simulation.sh

You can now edit this file according to the instructions inside. Press
``CTRL+X`` to save.

To run this, **in a tmux session**, in the ``my-sim1`` folder, run:

.. code-block:: console

    $ ./start-simulation.sh

At this point, you might get an message like:

.. code-block:: console

    Found some file names. Are these paths correct (if they match they should be correct)?
      - Actual:    C:\Users\shuhaowu\input_atstep=0-4-900.transformed.csv
      - Suspected: /files1/home/shuhaowu/simulations/my-sim1/input_atstep=0-4-900.transformed.csv (not found here)

    if so, type y here, otherwise please go adjust it and run this script again [y/N]:

The actual path is what the definition file defined. The suspected path is what
cfdbox detected as a possible path (it also says "file exists here" if the file
exists at the suspected path, otherwise it will say "not found here").

When you create a simulation in CFX-Pre under Windows, you might have used
a CSV file to define your initial/boundary conditions. CFX-Pre will **hardcode**
the path to the file in the definition file. This means if you run the CFX
solver on the cluster with the definition file created in Windows with such
a file, the simulation will not be able to start.

To get around this, you must modify the definition file on the server. You must
use a CFX command to export a ASCII command file from the definition file and
modify it. When you start a simulation with the ``cs-cfx`` executable we saw
earlier, it will automatically export this command file and check if the paths
are matching.

If the actual and suspected path are not the same or it says "not found here":

#. Make sure the profile file is at the suspected path.
#. Make sure to change the definition file by editing the command file (ccl file) with the instructions below.

To edit the CCL file:

.. code-block:: console

    $ nano YOUR_SIMULATION.def.ccl

Find any references to paths like ``C:\Users\...`` and replace it with the
suspected path.

Press ``CTRL-X`` to save.

Now in terminal, use the command:

.. code-block:: console

    $ cfx5cmds -write -def YOUR_SIMULATION.def -text YOUR_SIMULATION.def.ccl

This writes the command back into the definition file and you are ready to run
your simulation.

You might get a message such as:

.. code-block::

    Found a file at my-sim1.def.ccl. This file must be overwritten from a copy generated from the current definition file
    If you have unsaved changes, select N for the following option.
    overwrite my-sim1.def.ccl? (must select yes to run simulation) [y/N]

If you did not save your changes with ``cfx5cmds -write`` such as above, enter ``N``. Otherwise enter ``y``. cfdbox will recheck your work by overwriting the ccl file on disk.

Once everything is good, you should see a bunch of messages that potentially ends with:

.. code-block::

    Adding host amd32n1 (linux-amd64) to the parallel environment.

If you see the following, you can ignore it:

.. code-block::

    stty: standard input: Invalid argument
    This may cause problems spawning parallel slaves, especially on Windows

Now you may **detach** from the tmux session with ``CTRL+b d`` and then
disconnect. To go back and check the simulation, remember to use ``tmux attach``
when you log in to the server.