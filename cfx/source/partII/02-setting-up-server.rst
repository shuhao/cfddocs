Setting up the Server for Run
-----------------------------

Cluster Setup
+++++++++++++

It is a good time to get familiar with the clusters available here at Carleton.
This information is up to date as of the last update time of this document
(check cover page of the PDF or the bottom of the HTML page). However situation
may have changed since then.

There are currently 3 simulation clusters available: ``amd64``, ``amd32n1``,
and ``amd32n2``. ``amd64`` is much faster than ``amd32n1`` and ``amd32n2`` at
the time of writing.

Each cluster has a storage capacity of 1TB. The memory capacity is
256GB, 128GB, and 128GB respectively.

From within the department's network (any MAAE lab computers but not your
personal computer as it is not directly inside the MAAE network), you can access
them at the following IP address:

* ``amd64``: ``134.117.70.60``
* ``amd32n2``: ``134.117.70.92``
* ``amd32n1``: ``134.117.70.91``

You cannot access these machines from outside the department network without
the use of a jumphost. This is outside the scope of this tutorial and you should ask
the IT manager (probably Neil) to tell you if you need this.

There's a **NAS** (network attached storage) box on the network as well. At the
time of writing, this is a server where we have a total of 4 different storage
volumes each with 8TB of storage totally 32TB of storage space. However, the
actually amount available may be far less due to it storing all simulations
from this group.

The NAS is available at ``134.117.70.95``. It is also not directly accessible
outside the department network.

In order to connect to these boxes, you will need to be familiar with **SSH**.
If you're on a school computer, there is a program named ``Secure Shell Client``
preinstalled. This tutorial will outline how to connect to the cluster using
this setup.

If you're on your own computer, on Windows, you can download PuTTY_ for free.
On Unix based systems such as Mac OS X and Linux, SSH usually comes preinstalled
and can be accessed from terminal with the command ``ssh``.

.. _PuTTY: http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html

You should also familiarize yourself with basic Linux shell operations. A good
cheatsheet is provided here: http://cli.learncodethehardway.org/bash_cheat_sheet.pdf.
There are many more tutorials on the internet if you searched for it.

Logging into the Server (Windows, School Computer)
++++++++++++++++++++++++++++++++++++++++++++++++++

First, locate Secure Shell Client on the computer and start it up. You should be
getting a screen like the following:

.. image:: _static/ssh-secure-client-interface.png

Then you can click the ``Quick Connect`` button and be presented with this
screen:

.. image:: _static/ssh-secure-client-connect.png

Fill the host with the cluster you would like to connect to (for ``amd64`` it
is ``134.117.70.60``), and the username with your username. Leave the rest as
the default and hit ``Connect``. Enter your password when prompted. You will
then be presented with the following:

.. image:: _static/ssh-secure-shell-logged-in.png

Copying Files to Server (Windows, from School Computer)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

Click ``New File Transfer`` under the ``Window`` menu:

.. image:: _static/ssh-secure-client-new-file-transfer.png

A window like this will show up:

.. image:: _static/ssh-secure-shell-file-transfer.png

Just drag and drop in this window.

Setting Up ``cfdbox``
+++++++++++++++++++++

``cfdbox`` is a software package that I wrote to help you run and post process
CFD simulations. The scope of most of the code in this package is outside the
scope of this manual, but we will be using it server capabilities to help us
simplify runs and detect error conditions early if possible.

This software needs to be installed to your user account when you first login
to this server. You only need to do this **once**. After that you do not need
to reinstall unless you need to upgrade.

The software is open source licensed under AGPLv3 and the latest source code
is available at https://gitlab.com/shuhao/cfdbox.

**Note: This documentation will be using notation of $ <command> to show
commandline commands. You do not enter the $ in front of the command. It is a
notation that the command is something you typed and executed.**

So after you log in to the simulation server, in your home directory, you should
download cfdbox:

.. code-block:: console

    $ cd $HOME
    $ git clone https://gitlab.com/shuhao/cfdbox.git # this step can be skipped if you already did this previously
    $ cd cfdbox
    $ git checkout v0.1

Note the ``git checkout v<version_number>`` above may be incorrect, this
document describes the process for version |version|.

Then, to setup the server:

    $ scripts/bootstrap-server

This will ask you a bunch of questions. You can leave most of it default except
for the following two questions:

* The default nodes you wish your simulations to run on: this should be ``amd32n1*15`` for amd32n1, ``amd32n2*15`` for amd32n2, and ``amd64n1*30,amd64n2*30`` for amd64.
* The license path for CFX: this should be ``1055@134.117.71.28``

For the other questions:

* The directory where cfdxbox-server should not be changed and be in your home directory (``/home/<your-username>``).
* The directory CFX is installed to should be changed if we upgrade CFX version again.

The system will prompt you again to see if the settings are correct. As long as you see
the values you've entered in the debug output, enter ``y`` to confirm the installation.

At this point, the installation should take some time and at the end you should see
something like:

.. code-block::

    + python3 --version
    Python 3.5.1
    + tmux -V
    tmux 2.1
    INSTALLATION COMPLETE!

At this point, you should log out and log back in to the server to activate the
effects of cfdbox-server. When you log in, you should see ``(venv)`` in the
beginning of your terminal session like this:

.. code-block:: console

    (venv)[shuhaowu@amd64n1 ~]$

To test if everything works, execute ``cs-cfx`` on your command line and you
should see something like this:

.. code-block:: console

    (venv)[shuhaowu@amd64n1 ~]$ cs-cfx
    usage: cs-cfx [-h]
              {sftp-get,trn-mover,local-export,sftp-export,actual-run,run,monitor}
              ...
    cs-cfx: error: too few arguments
